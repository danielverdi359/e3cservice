
class BadRequestDocumentError(Exception):

    def __init__(self):

        message = '{' + \
            '"failure":{' + \
            '{' + \
            '"code": "elg.request.invalid=Invalid request message",' + \
            '"text": "Document empty!",' + \
            '"params": [],' + \
            '"detail": {' + \
            '}' + \
            '}' + \
            '}'

        super().__init__(message)

class BadRequestLanguageError(Exception):

    def __init__(self, language):

        message = '{' + \
            '"failure":{' + \
            '{' + \
            '"code": "elg.request.invalid=Invalid request message",' + \
            '"text": "Language not found!' + \
            '"params": [' + language + '],' + \
            '"detail": {' + \
            '}' + \
            '}' + \
            '}'

        super().__init__(message)


class InternalErrorDuringProcessing(Exception):

    def __init__(self):

        message = '{' + \
            '"failure":{' + \
            '{' + \
            '"code": "elg.service.internalError=Internal error during processing: {0}",' + \
            '"text": "Internal error!!",' + \
            '"params": [],' + \
            '"detail": {' + \
            '}' + \
            '}' + \
            '}'

        super().__init__(message)
