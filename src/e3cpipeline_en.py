import nltk
from nltk.tokenize import sent_tokenize
from transformers import pipeline

class E3CPipelineEN():

    model_name = '../models/EN/'


    def __init__(self):

        super().__init__()
        #nltk.download('punkt')
        self.nlp = pipeline(task="ner", model=E3CPipelineEN.model_name, tokenizer=E3CPipelineEN.model_name, framework="pt", grouped_entities=True)


    def annotate(self, document):

        # 1) splitting the document into sentences:
        sequence = sent_tokenize(document)

        # 2) extracting the entities from the sentences
        result = self.nlp(sequence)

        # adding two new attributes to the entity_i: start and end (their values are set to '0')
        # add the attribute start and attribute end to each entity (the start/end character position of the extracted entities)
        for entity in result:
            for word in entity:
                word["start"] = 0
                word["end"] = 0

        return result


if __name__ == "__main__":

    #testing the class
    e3cpipeline = E3CPipelineEN()
    result = e3cpipeline.annotate("The patient was previously diagnised with claudication. Jose reports abdominal pain. Today, Sachin won't be playing in the cricket match between India and Pakistan. The phase-III human clinical trial of the COVID-19 vaccine developed by Oxford University, and being manufactured by the Serum Institute of India (SII), will begin at the Sassoon General Hospital in Pune next week. On 24 June I will be travelling to Goa. Russian Government is trying hard to convince the world that their vaccine is effective. Pharma company Astrazeneca has released a 111-page trial blueprint on 21 Sept 2020.")
    print(result)