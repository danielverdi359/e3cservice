import pandas as pd

# load dataset
data_path = '../../../data/EN/raw/e3c_dataset.csv'
df = pd.read_csv(data_path, encoding="utf-8")

# fille the empty Sentence# with the previous available value
df.loc[:, "Sentence #"] = df["Sentence #"].fillna(method="ffill")
print(df.head())


# split dataset
sentences = df.groupby("Sentence #")["Word"].apply(list).values
tags = df.groupby("Sentence #")["Tag"].apply(list).values

from sklearn.model_selection import train_test_split
#split into 80% train and 20% test
X_train, X_test, y_train, y_test = train_test_split(sentences, tags, test_size=0.2, random_state=42)


# write preporocessed dataset
TRAIN_FILE_PATH = '../../../data/EN/processed/train.txt'
TEST_FILE_PATH = '../../../data/EN/processed/test.txt'
LABELS_FILE_PATH = '../../../data/EN/processed/labels.txt'

with open(TRAIN_FILE_PATH,'w',encoding="utf-8") as ftrain:
  for (k,v) in zip(X_train, y_train):
    [ftrain.write(s+' '+t+'\n') for s,t in zip(k,v)]
    ftrain.write('\n')

with open(TEST_FILE_PATH,'w',encoding="utf-8") as ftest:
  for (k,v) in zip(X_test, y_test):
    [ftest.write(s+' '+str(t)+'\n') for s,t in zip(k,v)]
    ftest.write('\n')

with open(LABELS_FILE_PATH,'w',encoding="utf-8") as f:
  for tag in df['Tag'].unique():
    print(tag)
    f.write(str(tag)+'\n')

