# documentation: https://european-language-grid.readthedocs.io/en/latest/all/3_Contributing/Service.html
# service example: https://live.european-language-grid.eu/catalogue/tool-service/480
from flask import Flask
from flask import render_template
from flask import request
from e3cpipeline import E3CPipeline

# creates a Flask application, named app
app = Flask(__name__)


# a route where we will display a welcome message via an HTML template
@app.route("/", methods=['POST', 'GET'])
def annotate():

    language = "en"  # at first there will be different services for each language
    if request.method == 'POST':
        text = request.form['text']
        # language = request.form['language']
    else:
        text = request.args.get('text')
        # language = request.args.get('language')

    text_request = {
        "type": "text",
        "params": {"language": language},
        "content": text
    }

    e3cpipeline = E3CPipeline(text_request)
    result = e3cpipeline.annotate(text_request)

    return result


# run the application
if __name__ == "__main__":
    app.run(debug=True)