from e3cpipeline_en import E3CPipelineEN
from e3cexception import BadRequestDocumentError
from e3cexception import BadRequestLanguageError
from e3cexception import InternalErrorDuringProcessing

class E3CPipeline:

    def __init__(self, text_request):

        self.language = text_request["params"]["language"]
        # Failure message
        if self.language is None or self.language != "en":
            raise BadRequestLanguageError(self.language)

        if self.language == "en":
            self.e3cpipeline = E3CPipelineEN
        #elif self.language == "it":
        #elif:

    def annotate(self, text_request):

        document = text_request["content"]
        # Failure message
        if document is None or document == "":
            raise BadRequestDocumentError()

        #run the classifier
        try:

            classification = self.e3cpipeline(document)

        except:
            raise InternalErrorDuringProcessing()

        #create the basic format for the ELG platform
        result = {
        "response":{
          "type":"annotations",
          "annotations":{
            "entities":[]
            }
          }
        }

        try:

            #add the results from the classification to the "entities" list in the response
            for sentence in classification:
                for word in sentence:
                    result['response']['annotations']['entities'].append({
                    "start":word["start"],
                    "end":word["end"],
                    "features":{
                        word['entity'],
                        word['score'],
                        word['word'],
                        "C0000000"  # the default entity ID
                      }
                    })

        except:
            raise InternalErrorDuringProcessing()

        return result

