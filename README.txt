|
| - data
|    | - raw (it contains the dataset)
|    | - processed (the processed dataset)
|
| - models (the trained models)
|    | - EN (the trained model you have created with google colab, i.e. 8 files in e3c_model)
|
|
| - src (it contains the classifiers and the service)
|    |
|    | - data (the script to process the raw dataset in input and produce the processed data)
|    | - models (the script to train the classifier)
